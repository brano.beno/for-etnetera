# Simple REST API server for JavaScript framework records handling

[![pipeline status](https://gitlab.com/brano.beno/for-etnetera/badges/master/pipeline.svg)](https://gitlab.com/brano.beno/for-etnetera/-/commits/master)
[![coverage report](https://gitlab.com/brano.beno/for-etnetera/badges/master/coverage.svg)](https://gitlab.com/brano.beno/for-etnetera/-/commits/master)

## API description
The following endpoints are available for usage:
1) **GET /framework/list** - returns list of all available records or empty list when no records have been found
2) **GET /framework/search?name={frameworkName}** - returns list of all available records with matching framework name or empty list in case of searched name doesn't match to any record
3) **POST /framework/add** - adds new record. Addition is refused when a record with the same name and version already exists
4) **POST /framework/update** - updates existing record. Update is refused when a record with the same name and version, but different ID already exists or when a record with the given ID doesn't exist at all
5) **DELETE /framework/delete/{frameworkId}** - deletes record with given ID. Deletion is refused when record with given ID doesn't exist at all

## Request body examples
##### For update
{
    "id": 1,
    "name": "Vue.js",
    "version": "2.6.10",
    "deprecationDate": "2022-02-15",
    "hypeLevel": "Mind blowing"
}

##### For addition - same as for update but without "id" field
{
    "name": "React",
    "version": "16.1.1",
    "deprecationDate": "2023-04-21",
    "hypeLevel": "Steady"
}

## Following request body fields are validated
- **name** - is not allowed to be null
- **hypeLevel** - acceptable entries are: _Unbearable_, _Tickling_, _Steady_, _Irresistible_ and _Mind blowing_

## Response body example in case of refused addition/update/delete operation or when validation constraints are violated
{
    "violations":
    [
        {
            "fieldName":"hypeLevel",
            "message":"Value is not allowed."
        },
        {
            "fieldName":"name",
            "message":"must not be null"
        }
    ]
}

{
    "violations":
    [
        {
            "fieldName":"Whole entity",
            "message":"Framework 'Angular' with version '1.0.1' already exists."
        }
    ]
}

{
    "violations":
    [
        {
            "fieldName":"Whole entity",
            "message":"Framework record with id '10' not found."
        }
    ]
}
