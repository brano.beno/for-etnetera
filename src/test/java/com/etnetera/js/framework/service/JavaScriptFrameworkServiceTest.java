package com.etnetera.js.framework.service;

import com.etnetera.js.framework.dto.JavaScriptFrameworkDto;
import com.etnetera.js.framework.model.JavaScriptFramework;
import com.etnetera.js.framework.repository.JavaScriptFrameworkRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JavaScriptFrameworkServiceTest {

  @Mock
  private JavaScriptFrameworkRepository repository;

  @InjectMocks
  private JavaScriptFrameworkService service;

  @Test
  @DisplayName("Test 'find all' happy path")
  void testFindAll() {
    when(repository.findAll()).thenReturn(List.of(new JavaScriptFramework()));
    Iterable<JavaScriptFramework> result = service.findAll();
    verify(repository).findAll();

    assertThat(result).hasSize(1);
  }

  @Test
  @DisplayName("Test whether 'exists by id' returns true")
  void testExistsById() {
    when(repository.existsById(anyLong())).thenReturn(true);
    boolean result = service.existsById(1L);
    verify(repository).existsById(anyLong());

    assertThat(result).isTrue();
  }

  @Test
  @DisplayName("Test whether 'find by name' returns list of JavaScriptFramework objects")
  void testFindByNameWithNotEmptyResult() {
    Iterable<JavaScriptFramework> result = provideFinByNameResult(List.of(new JavaScriptFramework()));

    assertThat(result).hasSize(1);
  }

  @Test
  @DisplayName("Test whether 'find by name' returns empty list")
  void testFindByNameWithEmptyResult() {
    Iterable<JavaScriptFramework> result = provideFinByNameResult(Collections.emptyList());

    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("Test whether 'find by name and version' returns JavaScriptFramework object")
  void testFindByNameAndVersionWithNotNullResult() {
    JavaScriptFramework result = provideFinByNameAndVersionResult(List.of(new JavaScriptFramework()));

    assertThat(result).isNotNull();
  }

  @Test
  @DisplayName("Test whether 'find by name and version' returns null")
  void testFindByNameAndVersionWithNullResult() {
    JavaScriptFramework result = provideFinByNameAndVersionResult(Collections.emptyList());

    assertThat(result).isNull();
  }

  @Test
  @DisplayName("Test whether 'exists by dto' returns true")
  void testExistsByDto() {
    when(repository.existsByNameIgnoreCaseAndVersionIgnoreCase(anyString(), anyString()))
            .thenReturn(true);
    boolean result = service.existsByDto(provideDto());
    verify(repository).existsByNameIgnoreCaseAndVersionIgnoreCase(anyString(), anyString());

    assertThat(result).isTrue();
  }

  @Test
  @DisplayName("Test 'add' happy path")
  void testAdd() {
    when(repository.save(any(JavaScriptFramework.class))).thenReturn(new JavaScriptFramework());
    JavaScriptFramework result = service.add(provideDto());
    verify(repository).save(any(JavaScriptFramework.class));

    assertThat(result).isNotNull();
  }

  @Test
  @DisplayName("Test 'update' happy path")
  void testUpdate() {
    when(repository.findById(anyLong())).thenReturn(Optional.of(new JavaScriptFramework()));
    when(repository.save(any(JavaScriptFramework.class))).thenReturn(new JavaScriptFramework());
    JavaScriptFramework result = service.update(provideDto());
    verify(repository).save(any(JavaScriptFramework.class));

    assertThat(result).isNotNull();
  }

  @Test
  @DisplayName("Test 'delete' happy path")
  void testDeleteById() {
    doNothing().when(repository).deleteById(anyLong());
    service.deleteById(1L);
    verify(repository).deleteById(anyLong());
  }

  private Iterable<JavaScriptFramework> provideFinByNameResult(List<JavaScriptFramework> list) {
    when(repository.findByNameIgnoreCase(anyString())).thenReturn(list);
    Iterable<JavaScriptFramework> result = service.findByName("Angular");
    verify(repository).findByNameIgnoreCase(anyString());
    return result;
  }

  private JavaScriptFramework provideFinByNameAndVersionResult(List<JavaScriptFramework> list) {
    when(repository.findByNameIgnoreCaseAndVersionIgnoreCase(anyString(), anyString()))
        .thenReturn(list);
    JavaScriptFramework result = service.findByNameAndVersion("Angular", "1.0");
    verify(repository).findByNameIgnoreCaseAndVersionIgnoreCase(anyString(), anyString());
    return result;
  }

  private JavaScriptFrameworkDto provideDto() {
    return new JavaScriptFrameworkDto(1, "Vue.js", "2.0", LocalDate.now(), "Steady");
  }
}