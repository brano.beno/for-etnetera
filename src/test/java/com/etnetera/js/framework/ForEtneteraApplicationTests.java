package com.etnetera.js.framework;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ForEtneteraApplicationTests {

  @Test
  @DisplayName("Test whether application context is established successfully")
  void contextLoads() {
  }
}
