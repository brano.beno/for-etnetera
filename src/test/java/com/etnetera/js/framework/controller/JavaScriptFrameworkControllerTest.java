package com.etnetera.js.framework.controller;

import com.etnetera.js.framework.dto.JavaScriptFrameworkDto;
import com.etnetera.js.framework.exception.FrameworkFoundException;
import com.etnetera.js.framework.exception.FrameworkNotFoundException;
import com.etnetera.js.framework.model.JavaScriptFramework;
import com.etnetera.js.framework.service.EntityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(JavaScriptFrameworkController.class)
class JavaScriptFrameworkControllerTest {

    @MockBean
    private EntityService<JavaScriptFramework, JavaScriptFrameworkDto> service;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Test scenario with response in XML format")
    void testShouldNotReturnXML() throws Exception {
        this.mockMvc
                .perform(get("/framework/list")
                        .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    @DisplayName("Test whether '/list' request succeeds")
    void testFindAll() throws Exception {
        when(service.findAll()).thenReturn(List.of(new JavaScriptFramework()));
        this.mockMvc
                .perform(get("/framework/list")
                        .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", is(1)))
                .andDo(print())
                .andReturn();
        verify(service).findAll();
    }

    @Test
    @DisplayName("Test whether '/search' request succeeds")
    void testFindByName() throws Exception {
        when(service.findByName(anyString())).thenReturn(List.of(new JavaScriptFramework()));
        this.mockMvc
                .perform(get("/framework/search?name=angular")
                        .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(1)));
        verify(service).findByName(anyString());
    }

    @Test
    @DisplayName("Test whether '/add' request succeeds")
    void testAdd() throws Exception {
        when(service.existsByDto(any(JavaScriptFrameworkDto.class))).thenReturn(false);
        when(service.add(any(JavaScriptFrameworkDto.class))).thenReturn(new JavaScriptFramework());
        String body = objectMapper.writeValueAsString(provideDto());
        this.mockMvc
                .perform(post("/framework/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andExpect(status().isCreated())
                .andExpect(header().exists("Content-Type"))
                .andExpect(header().string("Content-Type", Matchers.equalTo("application/json")));
        verify(service).existsByDto(any(JavaScriptFrameworkDto.class));
        verify(service).add(any(JavaScriptFrameworkDto.class));
    }

    @Test
    @DisplayName("Test whether '/add' request fails due to constraint violation")
    void testFailingAddDueConstraintViolation() throws Exception {
        when(service.existsByDto(any(JavaScriptFrameworkDto.class))).thenReturn(false);
        when(service.add(any(JavaScriptFrameworkDto.class))).thenThrow(ConstraintViolationException.class);
        String body = objectMapper.writeValueAsString(provideDto());
        this.mockMvc
                .perform(post("/framework/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andExpect(status().isBadRequest());
        verify(service).existsByDto(any(JavaScriptFrameworkDto.class));
        verify(service).add(any(JavaScriptFrameworkDto.class));
    }

    @Test
    @DisplayName("Test whether '/add' request fails due to date time parse exception")
    void testFailingAddDueDateTimeParseException() throws Exception {
        when(service.existsByDto(any(JavaScriptFrameworkDto.class))).thenReturn(false);
        when(service.add(any(JavaScriptFrameworkDto.class))).thenThrow(DateTimeParseException.class);
        String body = objectMapper.writeValueAsString(provideDto());
        this.mockMvc
                .perform(post("/framework/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andExpect(status().isBadRequest());
        verify(service).existsByDto(any(JavaScriptFrameworkDto.class));
        verify(service).add(any(JavaScriptFrameworkDto.class));
    }

    @Test
    @DisplayName("Test whether '/add' request fails due to duplicate adding")
    void testFailingAddDueDuplicate() throws Exception {
        when(service.existsByDto(any(JavaScriptFrameworkDto.class))).thenReturn(true);
        when(service.add(any(JavaScriptFrameworkDto.class))).thenThrow(FrameworkFoundException.class);
        String body = objectMapper.writeValueAsString(provideDto());
        this.mockMvc
                .perform(post("/framework/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andExpect(status().isFound());
        verify(service).existsByDto(any(JavaScriptFrameworkDto.class));
        verify(service, never()).add(any(JavaScriptFrameworkDto.class));
    }

    @Test
    @DisplayName("Test whether '/update' request fails due to not existing entity")
    void testFailingUpdateDueNotExistingEntity() throws Exception {
        when(service.existsById(anyLong())).thenReturn(false);
        when(service.update(any(JavaScriptFrameworkDto.class))).thenThrow(FrameworkNotFoundException.class);
        String body = objectMapper.writeValueAsString(provideDto());
        this.mockMvc
                .perform(post("/framework/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andExpect(status().isNotFound());
        verify(service).existsById(anyLong());
        verify(service, never()).update(any(JavaScriptFrameworkDto.class));
    }

    @Test
    @DisplayName("Test whether '/update' request fails due to duplicate adding")
    void testFailingUpdateDueDuplicate() throws Exception {
        when(service.existsById(anyLong())).thenReturn(true);
        when(service.findByNameAndVersion(anyString(), anyString())).thenReturn(new JavaScriptFramework());
        when(service.update(any(JavaScriptFrameworkDto.class))).thenThrow(FrameworkFoundException.class);
        String body = objectMapper.writeValueAsString(provideDto());
        this.mockMvc
                .perform(post("/framework/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andExpect(status().isFound());
        verify(service).existsById(anyLong());
        verify(service).findByNameAndVersion(anyString(), anyString());
        verify(service, never()).update(any(JavaScriptFrameworkDto.class));
    }

    @Test
    @DisplayName("Test whether '/update' request succeeds")
    void testUpdate() throws Exception {
        when(service.existsById(anyLong())).thenReturn(true);
        when(service.findByNameAndVersion(anyString(), anyString())).thenReturn(null);
        when(service.update(any(JavaScriptFrameworkDto.class))).thenReturn(new JavaScriptFramework());
        String body = objectMapper.writeValueAsString(provideDto());
        this.mockMvc
                .perform(post("/framework/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andExpect(status().isCreated())
                .andExpect(header().exists("Content-Type"))
                .andExpect(header().string("Content-Type", Matchers.equalTo("application/json")));
        verify(service).existsById(anyLong());
        verify(service).findByNameAndVersion(anyString(), anyString());
        verify(service).update(any(JavaScriptFrameworkDto.class));
    }

    @Test
    @DisplayName("Test whether '/delete' request succeeds")
    void testDelete() throws Exception {
        when(service.existsById(anyLong())).thenReturn(true);
        doNothing().when(service).deleteById(anyLong());
        this.mockMvc
                .perform(delete("/framework/delete/1"))
                .andExpect(status().isNoContent());
        verify(service).existsById(anyLong());
        verify(service).deleteById(anyLong());
    }

    @Test
    @DisplayName("Test whether '/delete' fails due to not existing entity")
    void testFailingDeleteDueNotExistingEntity() throws Exception {
        when(service.existsById(anyLong())).thenReturn(false);
        doNothing().when(service).deleteById(anyLong());
        this.mockMvc
                .perform(delete("/framework/delete/1"))
                .andExpect(status().isNotFound());
        verify(service).existsById(anyLong());
        verify(service, never()).deleteById(anyLong());
    }

    private JavaScriptFrameworkDto provideDto() {
        return new JavaScriptFrameworkDto(1, "Vue.js", "2.0", LocalDate.now(), "Steady");
    }
}