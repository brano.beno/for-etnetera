package com.etnetera.js.framework.repository;

import com.etnetera.js.framework.model.JavaScriptFramework;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class JavaScriptFrameworkRepositoryTest {

  @Autowired
  private JavaScriptFrameworkRepository repository;

  @BeforeEach
  void setUp() {
    assertThat(repository.count()).isZero();
  }

  @Test
  @DisplayName("Test whether find all operation succeeds")
  void testFindAll() {
    prepareEntityList();
    Iterable<JavaScriptFramework> result = repository.findAll();

    assertThat(result).hasSize(3);
  }

  @Test
  @DisplayName("Test whether find by name operation succeeds")
  void testFindByNameIgnoreCase() {
    prepareEntityList();
    List<JavaScriptFramework> result = repository.findByNameIgnoreCase("react");
    assertThat(result).hasSize(2);

    result = repository.findByNameIgnoreCase("node.js");
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("Test whether find by name and version operation succeeds")
  void testFindByNameByVersionIgnoreCase() {
    prepareEntityList();
    List<JavaScriptFramework> result = repository.findByNameIgnoreCaseAndVersionIgnoreCase("react", "16");
    assertThat(result).hasSize(1);

    result = repository.findByNameIgnoreCaseAndVersionIgnoreCase("node.js", "");
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("Test whether exists by name and version operation succeeds")
  void testExistsByNameByVersionIgnoreCase() {
    prepareEntityList();
    boolean result = repository.existsByNameIgnoreCaseAndVersionIgnoreCase("react", "16");
    assertThat(result).isTrue();

    result = repository.existsByNameIgnoreCaseAndVersionIgnoreCase("node.js", "");
    assertThat(result).isFalse();
  }

  @Test
  @DisplayName("Test whether save operation succeeds")
  void testSave() {
    JavaScriptFramework framework = provideFirstEntity();
    JavaScriptFramework result = repository.save(framework);

    assertThat(result).isNotNull();
  }

  @Test
  @DisplayName("Test whether delete by id operation succeeds")
  void testDeleteById() {
    JavaScriptFramework framework = repository.save(provideFirstEntity());
    long id = framework.getId();
    assertThat(repository.findById(id)).isPresent();

    repository.deleteById(id);

    assertThat(repository.findById(id)).isNotPresent();
  }

  private JavaScriptFramework provideFirstEntity() {
    JavaScriptFramework framework = new JavaScriptFramework();
    framework.setName("Angular");
    framework.setVersion("1.0");
    framework.setDeprecationDate(LocalDate.now());
    framework.setHypeLevel("Steady");
    return framework;
  }

  private JavaScriptFramework provideSecondEntity() {
    JavaScriptFramework framework = new JavaScriptFramework();
    framework.setName("React");
    framework.setVersion("15");
    framework.setDeprecationDate(LocalDate.now());
    framework.setHypeLevel("Tickling");
    return framework;
  }

  private JavaScriptFramework provideThirdEntity() {
    JavaScriptFramework framework = new JavaScriptFramework();
    framework.setName("React");
    framework.setVersion("16");
    framework.setDeprecationDate(LocalDate.now());
    framework.setHypeLevel("Tickling");
    return framework;
  }

  private void prepareEntityList() {
    repository.saveAll(List.of(provideFirstEntity(), provideSecondEntity(), provideThirdEntity()));
  }
}