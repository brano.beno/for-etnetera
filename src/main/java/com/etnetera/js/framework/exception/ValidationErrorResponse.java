package com.etnetera.js.framework.exception;

import java.util.Collection;
import java.util.List;

public final class ValidationErrorResponse {

  private final Collection<Violation> violations;

  public ValidationErrorResponse(Collection<Violation> violations) {
    this.violations = violations;
  }

  public Collection<Violation> getViolations() {
    return List.copyOf(violations);
  }
}
