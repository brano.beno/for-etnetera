package com.etnetera.js.framework.exception;

public class FrameworkFoundException extends RuntimeException {

  public FrameworkFoundException(String name, String version) {
    super(String.format("Framework '%s' with version '%s' already exists.", name, version));
  }
}
