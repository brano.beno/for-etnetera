package com.etnetera.js.framework.exception;

public class FrameworkNotFoundException extends RuntimeException {

  public FrameworkNotFoundException(long id) {
    super(String.format("Framework record with id '%d' not found.", id));
  }
}
