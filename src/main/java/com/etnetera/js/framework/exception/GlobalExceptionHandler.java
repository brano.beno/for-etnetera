package com.etnetera.js.framework.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> inputValidationException(ConstraintViolationException e) throws JsonProcessingException {
        String message = String.format("Invalid request: %s", e.getMessage());

        if (e.getConstraintViolations() != null) {
            List<Violation> violations = e.getConstraintViolations().stream()
                    .map(v -> new Violation(v.getPropertyPath().toString(), v.getMessage()))
                    .collect(Collectors.toUnmodifiableList());
            ValidationErrorResponse error = new ValidationErrorResponse(violations);
            message = objectMapper.writeValueAsString(error);
        }

        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<String> dateTimeParseException(DateTimeParseException e) throws JsonProcessingException {
        List<Violation> violations = List.of(new Violation("deprecationDate", e.getMessage()));
        ValidationErrorResponse error = new ValidationErrorResponse(violations);
        String message = objectMapper.writeValueAsString(error);

        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FrameworkFoundException.class)
    public ResponseEntity<String> frameworkFoundException(FrameworkFoundException e) throws JsonProcessingException {
        return handleFrameworkException(e.getMessage(), HttpStatus.FOUND);
    }

    @ExceptionHandler(FrameworkNotFoundException.class)
    public ResponseEntity<String> frameworkNotFoundException(FrameworkNotFoundException e) throws JsonProcessingException {
        return handleFrameworkException(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<String> handleFrameworkException(String exceptionMessage, HttpStatus httpStatus)
            throws JsonProcessingException {
        List<Violation> violations = List.of(new Violation("Whole entity", exceptionMessage));
        ValidationErrorResponse error = new ValidationErrorResponse(violations);
        String message = objectMapper.writeValueAsString(error);

        return new ResponseEntity<>(message, httpStatus);
    }
}
