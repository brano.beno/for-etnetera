package com.etnetera.js.framework.service;

public interface EntityService<T, U> {

  Iterable<T> findAll();

  boolean existsById(long id);

  Iterable<T> findByName(String name);

  T findByNameAndVersion(String name, String version);

  boolean existsByDto(U dto);

  T add(U dto);

  T update(U dto);

  void deleteById(long theId);
}