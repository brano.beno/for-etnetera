package com.etnetera.js.framework.service;

import com.etnetera.js.framework.dto.JavaScriptFrameworkDto;
import com.etnetera.js.framework.model.JavaScriptFramework;
import com.etnetera.js.framework.repository.JavaScriptFrameworkRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class JavaScriptFrameworkService implements EntityService<JavaScriptFramework, JavaScriptFrameworkDto> {

  private final JavaScriptFrameworkRepository repository;

  public JavaScriptFrameworkService(JavaScriptFrameworkRepository repository) {
    this.repository = Objects.requireNonNull(repository);
  }

  @Override
  public Iterable<JavaScriptFramework> findAll() {
    return repository.findAll();
  }

  @Override
  public boolean existsById(long id) {
    return repository.existsById(id);
  }

  @Override
  public Iterable<JavaScriptFramework> findByName(String name) {
    return repository.findByNameIgnoreCase(name);
  }

  @Override
  public JavaScriptFramework findByNameAndVersion(String name, String version) {
    return repository.findByNameIgnoreCaseAndVersionIgnoreCase(name, version)
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public boolean existsByDto(JavaScriptFrameworkDto dto) {
    return repository.existsByNameIgnoreCaseAndVersionIgnoreCase(dto.getName(), dto.getVersion());
  }

  @Override
  public JavaScriptFramework add(JavaScriptFrameworkDto dto) {
    return repository.save(convertToEntity(dto));
  }

  @Override
  public JavaScriptFramework update(JavaScriptFrameworkDto dto) {
    Optional<JavaScriptFramework> framework = repository.findById(dto.getId());
    return framework.map(f -> repository.save(updateEntity(dto, f))).orElse(null);
  }

  private JavaScriptFramework updateEntity(JavaScriptFrameworkDto dto, JavaScriptFramework entity) {
    entity.setName(dto.getName());
    entity.setVersion(dto.getVersion());
    entity.setDeprecationDate(dto.getDeprecationDate());
    entity.setHypeLevel(dto.getHypeLevel());
    return entity;
  }

  @Override
  public void deleteById(long id) {
    repository.deleteById(id);
  }

  private JavaScriptFramework convertToEntity(JavaScriptFrameworkDto dto) {
    var framework = new JavaScriptFramework();
    framework.setId(dto.getId());
    framework.setName(dto.getName());
    framework.setVersion(dto.getVersion());
    framework.setDeprecationDate(dto.getDeprecationDate());
    framework.setHypeLevel(dto.getHypeLevel());

    return framework;
  }
}
