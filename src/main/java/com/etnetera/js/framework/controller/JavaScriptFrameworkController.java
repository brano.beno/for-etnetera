package com.etnetera.js.framework.controller;

import com.etnetera.js.framework.dto.JavaScriptFrameworkDto;
import com.etnetera.js.framework.exception.FrameworkFoundException;
import com.etnetera.js.framework.exception.FrameworkNotFoundException;
import com.etnetera.js.framework.model.JavaScriptFramework;
import com.etnetera.js.framework.service.EntityService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.util.Objects;
import java.util.function.Supplier;

@RestController
@RequestMapping("/framework")
public class JavaScriptFrameworkController {

  private final EntityService<JavaScriptFramework, JavaScriptFrameworkDto> service;

  public JavaScriptFrameworkController(EntityService<JavaScriptFramework, JavaScriptFrameworkDto> service) {
    this.service = Objects.requireNonNull(service);
  }

  @GetMapping("/list")
  public Iterable<JavaScriptFramework> list() {
    return service.findAll();
  }

  @GetMapping("/search")
  public Iterable<JavaScriptFramework> search(@RequestParam("name") String name) {
    return service.findByName(name);
  }


  @PostMapping("/add")
  public ResponseEntity<JavaScriptFramework> add(@Valid @RequestBody JavaScriptFrameworkDto dto) {
    // check whether duplicate record is going to be added
    checkIsAddedDuplicate(dto);

    return saveEntity(() -> service.add(dto));
  }

  @PostMapping("/update")
  public ResponseEntity<JavaScriptFramework> update(@Valid @RequestBody JavaScriptFrameworkDto dto) {
    // check whether updated entity exists
    checkExists(dto);
    // check whether updated record is going to be duplicate
    checkIsUpdatedToDuplicate(dto);

    return saveEntity(() -> service.update(dto));
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Object> delete(@PathVariable long id) {
    // check whether deleted entity exists
    checkExists(id);

    service.deleteById(id);

    return ResponseEntity.noContent().build();
  }

  private void checkExists(long id) {
    if (!service.existsById(id)) {
      throw new FrameworkNotFoundException(id);
    }
  }

  private void checkExists(JavaScriptFrameworkDto dto) {
    checkExists(dto.getId());
  }

  private void checkIsAddedDuplicate(JavaScriptFrameworkDto dto) {
    if (service.existsByDto(dto)) {
      throw new FrameworkFoundException(dto.getName(), dto.getVersion());
    }
  }

  private void checkIsUpdatedToDuplicate(JavaScriptFrameworkDto dto) {
    JavaScriptFramework framework = service.findByNameAndVersion(dto.getName(), dto.getVersion());
    if (framework != null && framework.getId() != dto.getId()) {
      throw new FrameworkFoundException(dto.getName(), dto.getVersion());
    }
  }

  private ResponseEntity<JavaScriptFramework> saveEntity(Supplier<JavaScriptFramework> supplier) {
    JavaScriptFramework framework = supplier.get();
    var uri = ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}")
        .buildAndExpand(framework.getId())
        .toUri();

    return ResponseEntity.created(uri).body(framework);
  }
}
