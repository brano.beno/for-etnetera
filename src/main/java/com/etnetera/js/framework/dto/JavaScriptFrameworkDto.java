package com.etnetera.js.framework.dto;

import java.time.LocalDate;

public final class JavaScriptFrameworkDto {

  private final long id;
  private final String name;
  private final String version;
  private final LocalDate deprecationDate;
  private final String hypeLevel;

  public JavaScriptFrameworkDto(long id, String name, String version, LocalDate deprecationDate, String hypeLevel) {
    this.id = id;
    this.name = name;
    this.version = version;
    this.deprecationDate = deprecationDate;
    this.hypeLevel = hypeLevel;
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getVersion() {
    return version;
  }

  public LocalDate getDeprecationDate() {
    return deprecationDate;
  }

  public String getHypeLevel() {
    return hypeLevel;
  }
}
