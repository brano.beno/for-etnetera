package com.etnetera.js.framework.model;

import com.etnetera.js.framework.validator.HypeLevel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public class JavaScriptFramework {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @NotNull
  private String name;

  private String version;

  private LocalDate deprecationDate;

  @HypeLevel
  private String hypeLevel;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public LocalDate getDeprecationDate() {
    return deprecationDate;
  }

  public void setDeprecationDate(LocalDate deprecationDate) {
    this.deprecationDate = deprecationDate;
  }

  public String getHypeLevel() {
    return hypeLevel;
  }

  public void setHypeLevel(String hypeLevel) {
    this.hypeLevel = hypeLevel;
  }
}
