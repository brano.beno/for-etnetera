package com.etnetera.js.framework.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class HypeLevelValidator implements ConstraintValidator<HypeLevel, String> {

  final List<String> levels = List.of("Unbearable", "Tickling", "Steady", "Irresistible", "Mind blowing");

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return levels.contains(value);
  }
}
