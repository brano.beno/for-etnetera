package com.etnetera.js.framework.repository;

import com.etnetera.js.framework.model.JavaScriptFramework;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JavaScriptFrameworkRepository extends CrudRepository<JavaScriptFramework, Long> {

  List<JavaScriptFramework> findByNameIgnoreCase(String name);

  List<JavaScriptFramework> findByNameIgnoreCaseAndVersionIgnoreCase(String name, String version);

  boolean existsByNameIgnoreCaseAndVersionIgnoreCase(String name, String version);
}
