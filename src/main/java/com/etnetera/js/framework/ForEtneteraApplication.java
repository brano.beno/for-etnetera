package com.etnetera.js.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForEtneteraApplication {

  public static void main(String[] args) {
    SpringApplication.run(ForEtneteraApplication.class, args);
  }
}
